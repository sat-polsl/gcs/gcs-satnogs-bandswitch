FROM python:3.8.5-slim-buster

COPY requirements.txt /
RUN pip install --no-cache-dir -r requirements.txt

COPY satnogs-bandswitch.py /
COPY docker-entrypoint.sh /
COPY satnogs-bandswitch.conf /

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["/satnogs-bandswitch.py"]

