#!/bin/bash

set -e

SATNOGS_BANDSWITCH_CONFIG_FILE=/satnogs-bandswitch.conf
export PYTHONUNBUFFERED=TRUE

awk_script='BEGIN{
  c=0;
  for(v in ENVIRON) c++;
  i=0;
  for(v in ENVIRON) {
    if (v!="_") {
      printf "%s",v; if (i<c-1) printf "%s","\|"
    };
    i++
  }
}'

export $(grep -v '^#' $SATNOGS_BANDSWITCH_CONFIG_FILE | grep -v $(awk "$awk_script") | xargs)

exec "$@"

