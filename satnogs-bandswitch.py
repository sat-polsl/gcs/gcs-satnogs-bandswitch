#!/usr/local/bin/python3

import requests
import os
import docker
from os import environ
from distutils.util import strtobool
from time import sleep
from dateutil import parser
from dataclasses import dataclass
from datetime import datetime, timedelta, timezone

def _cast_or_none(func, value):
    try:
        return func(value)
    except (ValueError, TypeError):
        return None

SATNOGS_API_TOKEN = environ.get('SATNOGS_API_TOKEN', None)
SATNOGS_STATION_ID = _cast_or_none(int, environ.get('SATNOGS_STATION_ID', None))
SATNOGS_VERIFY_SSL = bool(strtobool(environ.get('SATNOGS_VERIFY_SSL', 'True')))
SATNOGS_NETWORK_API_URL = environ.get('SATNOGS_NETWORK_API_URL',
                                      'https://network.satnogs.org/api/')
SATNOGS_CLIENT_CONTAINER = environ.get('SATNOGS_CLIENT_CONTAINER', None)
SATNOGS_BANDSWITCH_TIMEOUT = _cast_or_none(int,environ.get('SATNOGS_BANDSWITCH_TIMEOUT', None))

BAND_2M_RF_INPUT = "TX/RX"
BAND_70CM_RF_INPUT = "RX2"

try:
    from urllib.parse import urljoin
except ImportError:
    from urlparse import urljoin

@dataclass
class Job:
    job_id: int
    start: datetime
    freq: int

def setup_docker_client():
    '''
    Returns instance of Docker client
    '''
    client = docker.DockerClient(base_url='unix://var/run/docker.sock')
    return client

def get_next_job():
    '''
    Requests SatNOGS network for frequency of next observation
    for station given in configuration
    '''
    url = urljoin(SATNOGS_NETWORK_API_URL,"jobs/")
    params = {
        'ground_station': SATNOGS_STATION_ID
    }
    headers = {'Authorization': 'Token {0}'.format(SATNOGS_API_TOKEN)}
    
    response = requests.get(url,
                            params=params,
                            headers=headers,
                            verify=SATNOGS_VERIFY_SSL,
                            timeout=45)

    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError as http_error:
        print(http_error)

    if len(response.json()) > 0:
        return response.json()[-1]

def update_job(current_job, next_job):
    '''
    Updates current job
    '''
    if next_job:
        start = parser.parse(next_job["start"])
        if current_job:
            if next_job["id"] != current_job.job_id:
                current_job = Job(next_job["id"], start, next_job["frequency"])
        else: 
            current_job = Job(next_job["id"], start, next_job["frequency"])
    else:
        current_job = None
    return current_job

def get_rf_input(satnogs_client):
    '''
    Return IF input used by satnogs-client in given container
    '''
    rf_input = satnogs_client.exec_run("/bin/bash -c 'echo $SATNOGS_ANTENNA'").output.decode("utf-8").rstrip()
    return rf_input

def get_rf_band(freq):
    if freq > 400000000:
        return BAND_70CM_RF_INPUT
    else:
        return BAND_2M_RF_INPUT

def reset_container(docker_client, satnogs_client, rf_input):
    print("stopping {0}...".format(SATNOGS_CLIENT_CONTAINER))
    satnogs_client.stop()
    print("removing {0}...".format(SATNOGS_CLIENT_CONTAINER))
    satnogs_client.remove()
    print("running {0}...".format(SATNOGS_CLIENT_CONTAINER))
    satnogs_client = docker_client.containers.run(SATNOGS_CLIENT_CONTAINER,
                                                  detach=True,
                                                  environment=["RIGCTLD_HOST=rigctld",
                                                               "RIGCTLD_PORT=4532",
                                                               "SATNOGS_ROT_MODEL=ROT_MODEL_NETROTCTL",
                                                               "SATNOGS_ROT_PORT=rotctld:4533",
                                                               "SATNOGS_ROT_FLIP=True",
                                                               "SATNOGS_ANTENNA={0}".format(rf_input)],
                                                  name=SATNOGS_CLIENT_CONTAINER,
                                                  restart_policy={"Name": "always"},
                                                  privileged=True,
                                                  init=True,
                                                  network="satnogs-network")
    return satnogs_client

def main():
    '''
    Main function for the band switch application. Reads freqency of next observation
    and configures satnogs client container for right RF input.
    '''
    current_job = None
    client = setup_docker_client()
    if not client:
        return

    satnogs_client = client.containers.get(SATNOGS_CLIENT_CONTAINER)

    while True:
        next_job = get_next_job()
        current_job = update_job(current_job, next_job)
        if current_job:
            print("Current RF input={0}, Next job RF input={1}".format(get_rf_input(satnogs_client),get_rf_band(current_job.freq)))
            if (current_job.start - datetime.now(timezone.utc)) < timedelta(minutes=SATNOGS_BANDSWITCH_TIMEOUT):
                if get_rf_band(current_job.freq) != get_rf_input(satnogs_client):
                    print("Resetting container.")
                    satnogs_client = reset_container(client, satnogs_client, get_rf_band(current_job.freq))

        print("Current job: {0}".format(current_job))
        sleep(60)

if __name__ == "__main__":
    main()
    

